# Generated by Django 4.0.3 on 2022-09-01 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meal_plans', '0002_remove_mealplan_date_mealplan_created'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mealplan',
            name='created',
        ),
        migrations.AddField(
            model_name='mealplan',
            name='date',
            field=models.DateTimeField(null=True),
        ),
    ]
